---
title: 'Understanding Encryption'
date: '26-10-2022 0:10'
taxonomy:
  category:
    tag: [howto]
    body_classes: 'single single-post'

---

Most important part of encryption is to understand its basic concept. This basic understanding of how encryption works will help you avoid confusion, issues and provide a more clear picture of what is happening and how to apply it to your daily communication.

Lacre does require you to understand the basics in order to use it without compromising security. It is built in a way that hides the encryption from users. However, it is better to understand what is happening. It empowers us all when realizing that there is no magic trickery, the concept is simple and you are in the front seat.

<iframe width="560" height="315" src="https://www.youtube.com/embed/E5FEqGYLL0o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

GPG (GNU Privacy Guard) is one of the most popular encryption methods to secure email correspondance and files between people. GnuPG uses asymmetric encryption technology. It means you create two types of keys. Secret and public key. Public key is used by others to encrypt messages to you. You can (and should) share that key with everyone you want to secure your communication with. There are even special public servers called "keyservers" where you can post your key so that anyone could obtain it. The second key is your secret (private) key. This key is used to decrypt emails addressed to you. Loosing or sharing this key with others will result in either loosing access to your emails or allowing others to read your private emails. It is therefore essential to never share this key with anyone and to keep a backup copy of it.

To create new pair of keys, please follow our tutorial pages at [Creating Keypairs](/user_tutorials/creating_keypair) Once you have your key pair created, it's time to start encrypting your emails. Here is a scenario:

Alice wants to write an email to Bob.
Both don't want the whole world to be able to read it.
So Alice will "hide" the email in an envelope closed with a lock that only Bob can open.

The problem is that Bob can't send the key to his lock to Alice through email, otherwise, anyone could use it to open his lock and read everything inside the envelope.

So here is the solution.

Bob creates two keys: one private, one public.
Alice also creates two keys: one private, one public.

Imagine the keys like this:
- the public key is like an open lock.
- the private key is like the key to the lock.

So Bob sends his open lock (= his public key) to Alice. Bob doesn't mind about the whole world seeing that, as it is public, it is just an open lock that holds nothing!

Alice writes the email that she wants to send to Bob, puts it in an envelope and closes it with Bob's lock (it is called "encrypting" an email). No one can read it, as no one has the key to open Bob's lock. Even Alice can not read it anymore!

Bob receives Alice's email in an envelope with the lock on. Bob can not read it, unless Bob uses the key to his own lock (= private key) to open the lock (using a key on the lock is called "decrypting" the email).

If Bob wants to answer Alice back, he can use her open lock (= public key) to close the envelope. She will use her key to the lock (= private key) to open it.

That's it. Although encryption algorythms and math behind it is very complex, the key concept as you can see isn't black magic, and is quite simple to understand.
