---
title: 'Android'
date: '23-10-2022 0:10'
taxonomy:
  category:
    tag: [howto]
    body_classes: 'single single-post'

---

Exporting your public key to upload it to lacre.io is super easy.

1. Open OpenKeychain and select your account.

![](01-copy-pub-andrd-start.png)

2. From the overview click on the "copy"~~-~~Symbol.

![](02-copy-pub-andrd-choose-key.png)

3. your public key is now added to the clipboard so you can paste it on [Lacre Keyserver](https://keys.lacre.io/?target=_blank).
