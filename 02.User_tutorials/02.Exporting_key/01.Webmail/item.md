---
title: 'Webmail'
date: '23-10-2022 0:10'
taxonomy:
  category:
    tag: [howto]
    body_classes: 'single single-post'

---
Exporting your public key to upload it to lacre.io is super easy.

1. Open your web browser then click on the Mailvelope icon in the top right corner of your browser. Choose "Keyring".

![](Mailvelope01.png)

2. Click on your key.

![](Mailvelope06.png)

3. From the overview click on "Export".

![](Mailvelope07.png)

4. Select "Public" and click on "Copy to cliboard".

![](Mailvelope07.png)

Your public key is now added to the clipboard so you can paste it on [Lacre Keyserver](https://keys.lacre.io/?target=_blank).
