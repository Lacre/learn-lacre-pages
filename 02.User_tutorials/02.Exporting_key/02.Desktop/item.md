---
title: 'Desktop'
date: '23-07-2022 0:10'
taxonomy:
  category:
    tag: [howto]
    body_classes: 'single single-post'

---
Exporting your public key using thunderbird is a very easy process. If you have followed our tutorial on creating a key pair, or have imported your old keypair to thunderbird, just follow these simple steps to export your public key.

1. Open up your account settings and head over to "End 2 end Ecryption" Section.

![](tb-e2e-1.png)

2. From the overview select the key you want to export

![](tb-export-1.png)

As you can see you can decide to either save the exported public key to a file or "Copy" the key to your Clipboard. You can do either one, but if you don't plan on uploading your public key directly, it's better to export to a file.

Exported public key can be then uploaded to Lacre Keyserver.
