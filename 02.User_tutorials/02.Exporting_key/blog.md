---
title: Exporting Keys
theme: learn4
hero_align: center
blog_url: how

sitemap:
    changefreq: monthly
    priority: 1.03

content:
    items:
    order:
        by: default
        dir: asc
    limit: 10
    pagination: true

feed:
    title: 'Lacre How-to'
    description: 'Lacre How-to'
    limit: 10

pagination: true
---

##Exporting Keypair

**In order to upload your key to Lacre's key server, you will first need to export it. See in this section how to do it from your device.**

[Webmail - Mailvelope](webmail)

[Desktop - Thunderbird](desktop)

[Android - K9 and OpenKeychain](android)

[iOS - COMING SOON...](ios)
