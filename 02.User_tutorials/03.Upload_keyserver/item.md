---
title: 'Upload to Lacre key server'
date: '23-10-2022 0:10'
taxonomy:
  category:
    tag: [howto]
    body_classes: 'single single-post'

---

Uploading a public key to the Lacre Key-Server is a fairly easy process:

1. Visit [keys.lacre.io](https://keys.lacre.io?target=_blank)

![](01-website.png)

2. Enter the email address you want to register. It should be the one you have created keys for.

![](02-email.png)

3. Paste the public key for that email address

![](03-pub-key.png)

4. Hit “submit key”

5. Wait for confirmation request to be sent to your address inbox and click on the received link.

6. Wait for confirmation of the registration to be sent to your address box

7. All done. From now on you will receive all emails to your inbox encrypted.


## Change key on Lacre Key-Server:

→ upload your new public key the same way as you did on initial setup.


## Remove key on Lacre Key-Server:

→ same as changing a key, but instead of adding a new key in the "ASCII-armored PGP public key" box, let it empty.
