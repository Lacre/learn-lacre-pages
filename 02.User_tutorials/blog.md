---
title: User Docs
theme: learn4
hero_align: center
blog_url: how

sitemap:
    changefreq: monthly
    priority: 1.03

content:
    items:
    order:
        by: default
        dir: asc
    limit: 0
    pagination: true

feed:
    title: 'Lacre User docs'
    description: 'Using Lacre'
    limit: 10

pagination: true
---

## User Documentation

**The following tutorials will guide you through the process of setting up encryption keys and uploading them to Lacre.**

[Creating Keypairs](creating_keypair)

[Exporting Your Keys](exporting_key)

[Uploading Your Key to Lacre's Keyserver](upload_keyserver)
