---
title: 'Webmail'
date: '19-10-2022 0:10'
taxonomy:
  category:
    tag: [howto,mailvelope,web-browser]
    body_classes: 'single single-post'

---

Creating PGP keys from your Web browser requires you to install an add-on.

So if you haven’t already, install Mailveloppe add-on in your web browser:

[Mailvelope for Google Chrome](https://chrome.google.com/webstore/detail/mailvelope/kajibbejlbohfaggdiogboambcijhkke?target=_blank)
<br>
[Mailvelope for Mozilla Firefox](https://addons.mozilla.org/en/firefox/addon/mailvelope/?target=_blank)
<br>
[Mailvelope for Microsoft Edge](https://microsoftedge.microsoft.com/addons/detail/mailvelope/dgcbddhdhjppfdfjpciagmmibadmoapc?target=_blank)


To create a new keypair for your email you need to:

1. open your web browser then click on the Mailvelope icon in the top right corner of your browser to start setting up your encryption keys. Choose "Keyring".

![](Mailvelope01.png)

2. select “Generate”.

![](Mailvelope02.png)

3. Fill in the needed information

- give a name associated with the key (usually the name of the key owner, but can be what you want)

- enter your email address that you want to use with the key. For Lacre, It should be the address you want to use with Lacre.

- you can change additional settings by clicking on "Advanced", like changing key type to Ecliptic Curve as it is considered more secure and efficient than RSA keys.

- enter a password for your private key. You'll be asked for it when you want to decrypt an email with your private key.

- Uncheck the "Upload public key to Mailvelope Key Server" box, unless you want it to be published online (it allows people to find your public key and send you encrypted emails, even if you didn't directly send your public key to them).

![](Mailvelope03.png)

4. Click on "Generate".

5. Congratulations! Your new encryption key is created and ready to be used.


Although you are technically done, always backup your keys. Loosing important data is devastating. Loosing the key to open all your email communication can be even worse. Have a backup stored safely offline, just in case.

- Select “Export” from the Key Management page.

![](Mailvelope04.png)

- Choose "Public" and give your public key backup file a name (it is good practice to have "_pub" in the file name to indicate that this is the public key, and "_priv" to indicate this is the private key).

![](Mailvelope05.png)

- Then click on "Save" and choose where to save the file. Do the same thing for your "Private" key.


With proper end to end encryption, you are responsible yourself for your data security. So the only person to blame for lost key or password is you! With great security comes great responsibility.
