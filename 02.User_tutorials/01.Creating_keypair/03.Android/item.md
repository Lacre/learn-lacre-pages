---
title: 'Android'
date: '25-10-2022 0:10'
taxonomy:
  category:
    tag: [howto, android, mobile]
    body_classes: 'single single-post'

---

Creating a PGP keys on your Android device requires you to install additional software. Android unfortunately isn't equipped with PGP.

So if you haven’t already, install OpenKeychain from [F-droid](https://f-droid.org/en/packages/org.sufficientlysecure.keychain/?target=_blank) or [Google Playstore](https://play.google.com/store/apps/details?id=org.sufficientlysecure.keychain?target=_blank)

To create a new keypair for your email you need to:

1. Open OpenKeychain and select “CREATE MY KEY” on start page.

![](image-openkeychain/01-create-andrd-start.png)

(If you already have keys you can get there also via the hamburger menu (three bars) at the top right → manage my keys)

2. Choose a name to associate with the key

![](image-openkeychain/02-create-andrd-username.png)

3. Enter the email address that you want to use with the key. For Lacre you will need to enter the same email address you plan to use with Lacre.

![](image-openkeychain/03-create-andrd-email.png)

4. you can change additional settings from the hamburger menu at the top right → “change key configuration”. You can change the key type to Ecliptic Curve as it is considered more secure and efficient then RSA keys, or you may directly go on with "CREATE KEY" if you are fine with the defaults.

![](image-openkeychain/04-create-andrd-create.png)
![](image-openkeychain/05-create-andrd-additional-settings.png)

5. Congratulations! Your new encryption key is created and ready to be used.

![](image-openkeychain/06-create-andrd-finished.png)

6. Although you are technically done, always backup your keys. Loosing important data is devastating. Loosing the key to open all your email communication can be even worse. Have a backup stored safely offline, just in case.

Select “Backup key” from the hamburger menu at the top right within the key you just created.

![](image-openkeychain/07-create-andrd-backup-start.png)

7. Note down password. If you loose the password to unlock your key, you will loose access to the key. So keep the password somewhere safe, for example use password manager.

![](image-openkeychain/08-create-andrd-backup-password.png)

Now click “SAVE BACKUP” and save on location of your choice.


With proper end to end encryption, you are responsible yourself for your data security. So the only person to blame for lost key or password is you! With great security comes great responsibility.


Final step will be to enable PGP within email client. Only few email clients on android support that. In this tutorial we’ll show you how to do it with K9.
If you are not using it already, install K9 from [F-Droid](https://f-droid.org/app/com.fsck.k9?target=_blank) or [Google Playstore](https://play.google.com/store/apps/details?id=com.fsck.k9?target=_blank)

1. Start K9, open the drawer and go to “Settings”.

![](image-k9/01-k9-start.png)

2. Select the account you created the key for.

![](image-k9/02-k9-choose-account.png)

3. Click “End-to-end encryption”.

![](image-k9/03-k9-account-settings.png)

4. Tick the slider button “Enable OpenPGP support” and select “Configure end-to-end key” afterwards.

![](image-k9/04-k9-enable-e2ee.png)

5. Finally click “use key: username@lacre.io” and you are all set up.

![](image-k9/05-k9-select-key.png)
