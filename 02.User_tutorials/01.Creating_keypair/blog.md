---
title: Creating keypair
theme: Learn-lacre-theme
hero_align: center
blog_url: how

sitemap:
    changefreq: monthly
    priority: 1.03

content:
    items:
    order:
        by: default
        dir: asc
    limit: 0
    pagination: false

feed:
    title: 'Creating keypair'
    description: 'How to create keypair using mail client'
    limit: 0

pagination: true
---

## Creating Keypairs

**To be able to decrypt your mailbox, you have to create your set of keys. The following section will guide you through the process.**

[Webmail - Mailvelope](webmail)

[Desktop - Thunderbird](desktop)

[Android - K9 and OpenKeychain](android)

[iOS - COMING SOON...](ios)
