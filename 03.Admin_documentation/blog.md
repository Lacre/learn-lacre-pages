---
title: Admin Docs

theme: learn4
hero_align: center
blog_url: how

sitemap:
    changefreq: monthly
    priority: 1.03

content:
    items: 
    order:
        by: header.date
        dir: desc
    limit: 10
    pagination: true

feed:
    title: 'Lacre How-to'
    description: 'Lacre How-to'
    limit: 10

pagination: true
---

##Coming soon!
